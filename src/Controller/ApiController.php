<?php

namespace App\Controller;

use App\Manager\BookingManager;
use App\Repository\BookRepository;
use App\Repository\UserRepository;
use Exception;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Attribute\MapQueryParameter;
use Symfony\Component\Routing\Attribute\Route;

class ApiController extends AbstractController
{
    public function __construct(
        private readonly BookRepository $bookRepository,
        private readonly UserRepository $userRepository,
        private readonly BookingManager $bookingManager,
    ){
    }

    #[Route('/api', name: 'app_api')]
    public function index(): JsonResponse
    {
        return $this->json([
            'message' => 'Welcome to API controller!',
        ]);
    }

    /**
     * @throws Exception
     */
    #[Route('/api/books', name: 'api_list', methods: ['GET'])]
    public function listBooks(
        #[MapQueryParameter] ?string $title,
        #[MapQueryParameter] ?string $category,
        #[MapQueryParameter] ?string $publishedAt,
    ): JsonResponse
    {
        if ($title || $category || $publishedAt) {
            $books = $this->bookRepository->listBooks($title, $category, $publishedAt);
        } else {
            $books = $this->bookRepository->findAll();
        }
        return $this->json($books, 200, [], ['groups' => 'book:read']);
    }

    #[Route('/api/booking', name: 'api_booking', methods: ['POST'])]
    public function booking(Request $request): JsonResponse
    {
        $book = $this->bookRepository->find($request->query->get('bookId'));
        $user = $this->userRepository->find($request->query->get('userId'));

        $booking = $this->bookingManager->createBooking($book, $user);
        return $this->json($booking, 200, [], ['groups' => 'booking:read']);
    }
}
