<?php

namespace App\Manager;

use App\Entity\Book;
use App\Entity\Booking;
use App\Entity\User;
use App\Repository\BookRepository;
use Doctrine\ORM\EntityManagerInterface;

class BookingManager
{
    public function __construct(
        private EntityManagerInterface $em,
    ){
    }

    public function createBooking(Book $bookId, User $userId): Booking
    {
        $booking = (new Booking())
            ->setUserId($userId)
            ->setBook($bookId)
        ;

        $this->em->persist($booking);
        $this->em->flush();

        return $booking;
    }
}