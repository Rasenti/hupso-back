<?php

namespace App\Repository;

use App\Entity\Book;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @extends ServiceEntityRepository<Book>
 *
 * @method Book|null find($id, $lockMode = null, $lockVersion = null)
 * @method Book|null findOneBy(array $criteria, array $orderBy = null)
 * @method Book[]    findAll()
 * @method Book[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class BookRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Book::class);
    }

    /**
     * @return Book[] Returns an array of Book objects
     * @throws \Exception
     */
    public function listBooks(
        ?string $title = null,
        ?string $category = null,
        ?string $publishedAtString = null,
    ): array {
        $queryBuilder = $this->createQueryBuilder('book');
        if ($title) {
            $queryBuilder
                ->andWhere('book.title LIKE :title')
                ->setParameter('title', '%'.$title.'%');
        }

        if ($category) {
            $queryBuilder
                ->andWhere('book.category = :category')
                ->setParameter('category', $category);
        }

        if ($publishedAtString) {
            $publishedAt = new \DateTimeImmutable($publishedAtString);
            $queryBuilder
                ->andWhere('book.publishedAt = :publishedAt')
                ->setParameter('publishedAt', $publishedAt);
        }

        return $queryBuilder
            ->getQuery()
            ->getResult()
            ;
    }
}
